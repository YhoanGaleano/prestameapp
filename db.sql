USE [master]
GO
/****** Object:  Database [prestameDB]    Script Date: 19/07/2016 10:42:01 p. m. ******/
CREATE DATABASE [prestameDB]
GO
USE [prestameDB]
GO
/****** Object:  Table [dbo].[DestrezaEmpleado]    Script Date: 19/07/2016 10:42:02 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DestrezaEmpleado](
	[idDestrezaEmpleado] [int] IDENTITY(1,1) NOT NULL,
	[idEmpleado] [int] NOT NULL,
	[idDestreza] [int] NOT NULL,
 CONSTRAINT [PK_DestrezaEmpleado] PRIMARY KEY CLUSTERED 
(
	[idDestrezaEmpleado] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Destrezas]    Script Date: 19/07/2016 10:42:02 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Destrezas](
	[idDestreza] [int] IDENTITY(1,1) NOT NULL,
	[nombre] [varchar](50) NOT NULL,
	[porcentajeDestreza] [tinyint] NOT NULL,
	[descripcion] [varchar](100) NOT NULL,
	[estado] [bit] NOT NULL,
 CONSTRAINT [PK_Destrezas] PRIMARY KEY CLUSTERED 
(
	[idDestreza] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Empleados]    Script Date: 19/07/2016 10:42:02 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Empleados](
	[idEmpleado] [int] IDENTITY(1,1) NOT NULL,
	[nombre] [varchar](40) NOT NULL,
	[fechaNacimiento] [date] NOT NULL,
	[genero] [tinyint] NOT NULL,
	[urlFoto] [varchar](50) NULL,
	[idUsuario] [int] NOT NULL,
	[estado] [tinyint] NOT NULL,
	[idEmpresa] [int] NOT NULL,
 CONSTRAINT [PK_Empleados] PRIMARY KEY CLUSTERED 
(
	[idEmpleado] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Empresas]    Script Date: 19/07/2016 10:42:02 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Empresas](
	[idEmpresa] [int] IDENTITY(1,1) NOT NULL,
	[nit] [varchar](12) NOT NULL,
	[razonSocial] [varchar](50) NOT NULL,
	[telefono] [varchar](15) NOT NULL,
	[nombreResponsable] [varchar](50) NOT NULL,
	[idUsuario] [int] NOT NULL,
	[estado] [tinyint] NOT NULL,
 CONSTRAINT [PK_Empresas] PRIMARY KEY CLUSTERED 
(
	[idEmpresa] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Entrada]    Script Date: 19/07/2016 10:42:02 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Entrada](
	[idEntrada] [int] IDENTITY(1,1) NOT NULL,
	[fechaEntrada] [date] NOT NULL,
	[idUsuario] [int] NOT NULL,
	[estado] [bit] NOT NULL,
 CONSTRAINT [PK_Entrada] PRIMARY KEY CLUSTERED 
(
	[idEntrada] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[EntradaUniforme]    Script Date: 19/07/2016 10:42:02 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EntradaUniforme](
	[idEntradaUniforme] [int] IDENTITY(1,1) NOT NULL,
	[idEntrada] [int] NOT NULL,
	[idUniforme] [int] NOT NULL,
	[cantidad] [int] NOT NULL,
	[precio] [decimal](6, 2) NOT NULL,
 CONSTRAINT [PK_EntradaUniforme] PRIMARY KEY CLUSTERED 
(
	[idEntradaUniforme] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[EstudioEmpleado]    Script Date: 19/07/2016 10:42:02 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EstudioEmpleado](
	[idEstudioEmpleado] [int] IDENTITY(1,1) NOT NULL,
	[idEmpleado] [int] NOT NULL,
	[idEstudio] [int] NOT NULL,
 CONSTRAINT [PK_EstudioEmpleado] PRIMARY KEY CLUSTERED 
(
	[idEstudioEmpleado] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Estudios]    Script Date: 19/07/2016 10:42:02 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Estudios](
	[idEstudio] [int] IDENTITY(1,1) NOT NULL,
	[nombre] [varchar](100) NOT NULL,
	[descripcion] [varchar](200) NULL,
	[nivelEstudio] [tinyint] NOT NULL,
	[graduado] [bit] NOT NULL,
	[fechaTerminacion] [date] NOT NULL,
	[nombreInstitucion] [varchar](100) NOT NULL,
	[estado] [bit] NOT NULL,
 CONSTRAINT [PK_Estudios] PRIMARY KEY CLUSTERED 
(
	[idEstudio] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Modulo]    Script Date: 19/07/2016 10:42:02 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Modulo](
	[idModulo] [int] IDENTITY(1,1) NOT NULL,
	[nombre] [varchar](50) NOT NULL,
	[nombreCorto] [varchar](20) NULL,
	[url] [varchar](40) NOT NULL,
	[padre] [bit] NOT NULL,
	[hijo] [bit] NOT NULL,
	[padreId] [int] NULL,
	[icono] [varchar](10) NULL,
	[estado] [bit] NOT NULL,
 CONSTRAINT [PK_Modulo] PRIMARY KEY CLUSTERED 
(
	[idModulo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ModuloRol]    Script Date: 19/07/2016 10:42:02 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ModuloRol](
	[idModuloRol] [int] IDENTITY(1,1) NOT NULL,
	[idModulo] [int] NOT NULL,
	[idRol] [int] NOT NULL,
 CONSTRAINT [PK_ModuloRol] PRIMARY KEY CLUSTERED 
(
	[idModuloRol] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Prestamos]    Script Date: 19/07/2016 10:42:02 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Prestamos](
	[idPrestamo] [int] IDENTITY(1,1) NOT NULL,
	[idEmpleado] [int] NOT NULL,
	[idEmpresa] [int] NOT NULL,
	[fechaInicio] [date] NOT NULL,
	[fechaFin] [date] NOT NULL,
	[actividades] [varchar](max) NOT NULL,
	[direccion] [varchar](50) NOT NULL,
	[estado] [tinyint] NOT NULL,
 CONSTRAINT [PK_Prestamos] PRIMARY KEY CLUSTERED 
(
	[idPrestamo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Roles]    Script Date: 19/07/2016 10:42:02 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Roles](
	[idRol] [int] IDENTITY(1,1) NOT NULL,
	[nombre] [varchar](50) NOT NULL,
	[estado] [bit] NOT NULL CONSTRAINT [DF_Rol_estado]  DEFAULT ((1)),
 CONSTRAINT [PK_Rol] PRIMARY KEY CLUSTERED 
(
	[idRol] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Salida]    Script Date: 19/07/2016 10:42:02 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Salida](
	[idSalida] [int] IDENTITY(1,1) NOT NULL,
	[idTipoSalida] [tinyint] NOT NULL,
	[idUsuario] [int] NOT NULL,
	[fechaSalida] [date] NOT NULL,
	[fechaEnvioTentativa] [date] NULL,
	[fechaDespacho] [date] NULL,
	[descripcion] [varchar](100) NULL,
	[idEmpresa] [int] NOT NULL,
	[urlComprobante] [varchar](50) NULL,
	[estado] [tinyint] NOT NULL,
 CONSTRAINT [PK_Salida] PRIMARY KEY CLUSTERED 
(
	[idSalida] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SalidaUniforme]    Script Date: 19/07/2016 10:42:02 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SalidaUniforme](
	[idSalidaUniforme] [int] IDENTITY(1,1) NOT NULL,
	[idSalida] [int] NOT NULL,
	[idUniforme] [int] NOT NULL,
	[cantidad] [int] NOT NULL,
	[precio] [decimal](6, 2) NOT NULL,
 CONSTRAINT [PK_SalidaUniforme] PRIMARY KEY CLUSTERED 
(
	[idSalidaUniforme] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Uniformes]    Script Date: 19/07/2016 10:42:02 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Uniformes](
	[idUniforme] [int] IDENTITY(1,1) NOT NULL,
	[nombre] [varchar](30) NOT NULL,
	[tipoUniforme] [tinyint] NOT NULL,
	[urlFotoFrontal] [varchar](50) NOT NULL,
	[urlFotoTrasera] [varchar](50) NOT NULL,
	[descripcion] [varchar](200) NULL,
	[precio] [decimal](6, 2) NOT NULL,
	[cantidadExistente] [int] NOT NULL,
	[cantidadDisponible] [int] NOT NULL,
	[estado] [bit] NOT NULL,
 CONSTRAINT [PK_Uniformes] PRIMARY KEY CLUSTERED 
(
	[idUniforme] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Usuarios]    Script Date: 19/07/2016 10:42:02 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Usuarios](
	[idUsuario] [int] IDENTITY(1,1) NOT NULL,
	[email] [varchar](30) NOT NULL,
	[usuario] [varchar](20) NOT NULL,
	[contrasenia] [varchar](100) NOT NULL,
	[contraseniaSalt] [varchar](100) NOT NULL,
	[idRol] [int] NOT NULL,
 CONSTRAINT [PK_Usuarios] PRIMARY KEY CLUSTERED 
(
	[idUsuario] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[Destrezas] ADD  CONSTRAINT [DF_Destrezas_estado]  DEFAULT ((1)) FOR [estado]
GO
ALTER TABLE [dbo].[Estudios] ADD  CONSTRAINT [DF_Estudios_graduado]  DEFAULT ((1)) FOR [graduado]
GO
ALTER TABLE [dbo].[Estudios] ADD  CONSTRAINT [DF_Estudios_estado]  DEFAULT ((1)) FOR [estado]
GO
ALTER TABLE [dbo].[Modulo] ADD  CONSTRAINT [DF_Modulo_padre]  DEFAULT ((1)) FOR [padre]
GO
ALTER TABLE [dbo].[Modulo] ADD  CONSTRAINT [DF_Modulo_hijo]  DEFAULT ((0)) FOR [hijo]
GO
ALTER TABLE [dbo].[Modulo] ADD  CONSTRAINT [DF_Modulo_estado]  DEFAULT ((1)) FOR [estado]
GO
ALTER TABLE [dbo].[Uniformes] ADD  CONSTRAINT [DF_Uniformes_estado]  DEFAULT ((1)) FOR [estado]
GO
ALTER TABLE [dbo].[DestrezaEmpleado]  WITH CHECK ADD  CONSTRAINT [FK_DestrezaEmpleado_Destrezas] FOREIGN KEY([idDestreza])
REFERENCES [dbo].[Destrezas] ([idDestreza])
GO
ALTER TABLE [dbo].[DestrezaEmpleado] CHECK CONSTRAINT [FK_DestrezaEmpleado_Destrezas]
GO
ALTER TABLE [dbo].[DestrezaEmpleado]  WITH CHECK ADD  CONSTRAINT [FK_DestrezaEmpleado_Empleados] FOREIGN KEY([idEmpleado])
REFERENCES [dbo].[Empleados] ([idEmpleado])
GO
ALTER TABLE [dbo].[DestrezaEmpleado] CHECK CONSTRAINT [FK_DestrezaEmpleado_Empleados]
GO
ALTER TABLE [dbo].[Empleados]  WITH CHECK ADD  CONSTRAINT [FK_Empleados_Empresas] FOREIGN KEY([idEmpresa])
REFERENCES [dbo].[Empresas] ([idEmpresa])
GO
ALTER TABLE [dbo].[Empleados] CHECK CONSTRAINT [FK_Empleados_Empresas]
GO
ALTER TABLE [dbo].[Empleados]  WITH CHECK ADD  CONSTRAINT [FK_Empleados_Usuarios] FOREIGN KEY([idUsuario])
REFERENCES [dbo].[Usuarios] ([idUsuario])
GO
ALTER TABLE [dbo].[Empleados] CHECK CONSTRAINT [FK_Empleados_Usuarios]
GO
ALTER TABLE [dbo].[Empresas]  WITH CHECK ADD  CONSTRAINT [FK_Empresas_Usuarios] FOREIGN KEY([idUsuario])
REFERENCES [dbo].[Usuarios] ([idUsuario])
GO
ALTER TABLE [dbo].[Empresas] CHECK CONSTRAINT [FK_Empresas_Usuarios]
GO
ALTER TABLE [dbo].[EntradaUniforme]  WITH CHECK ADD  CONSTRAINT [FK_EntradaUniforme_Entrada] FOREIGN KEY([idEntrada])
REFERENCES [dbo].[Entrada] ([idEntrada])
GO
ALTER TABLE [dbo].[EntradaUniforme] CHECK CONSTRAINT [FK_EntradaUniforme_Entrada]
GO
ALTER TABLE [dbo].[EntradaUniforme]  WITH CHECK ADD  CONSTRAINT [FK_EntradaUniforme_Uniformes] FOREIGN KEY([idUniforme])
REFERENCES [dbo].[Uniformes] ([idUniforme])
GO
ALTER TABLE [dbo].[EntradaUniforme] CHECK CONSTRAINT [FK_EntradaUniforme_Uniformes]
GO
ALTER TABLE [dbo].[EstudioEmpleado]  WITH CHECK ADD  CONSTRAINT [FK_EstudioEmpleado_Empleados] FOREIGN KEY([idEmpleado])
REFERENCES [dbo].[Empleados] ([idEmpleado])
GO
ALTER TABLE [dbo].[EstudioEmpleado] CHECK CONSTRAINT [FK_EstudioEmpleado_Empleados]
GO
ALTER TABLE [dbo].[EstudioEmpleado]  WITH CHECK ADD  CONSTRAINT [FK_EstudioEmpleado_Estudios] FOREIGN KEY([idEstudio])
REFERENCES [dbo].[Estudios] ([idEstudio])
GO
ALTER TABLE [dbo].[EstudioEmpleado] CHECK CONSTRAINT [FK_EstudioEmpleado_Estudios]
GO
ALTER TABLE [dbo].[ModuloRol]  WITH CHECK ADD  CONSTRAINT [FK_ModuloRol_Modulo] FOREIGN KEY([idModulo])
REFERENCES [dbo].[Modulo] ([idModulo])
GO
ALTER TABLE [dbo].[ModuloRol] CHECK CONSTRAINT [FK_ModuloRol_Modulo]
GO
ALTER TABLE [dbo].[ModuloRol]  WITH CHECK ADD  CONSTRAINT [FK_ModuloRol_Roles] FOREIGN KEY([idRol])
REFERENCES [dbo].[Roles] ([idRol])
GO
ALTER TABLE [dbo].[ModuloRol] CHECK CONSTRAINT [FK_ModuloRol_Roles]
GO
ALTER TABLE [dbo].[Prestamos]  WITH CHECK ADD  CONSTRAINT [FK_Prestamos_Empleados] FOREIGN KEY([idEmpleado])
REFERENCES [dbo].[Empleados] ([idEmpleado])
GO
ALTER TABLE [dbo].[Prestamos] CHECK CONSTRAINT [FK_Prestamos_Empleados]
GO
ALTER TABLE [dbo].[Prestamos]  WITH CHECK ADD  CONSTRAINT [FK_Prestamos_Empresas] FOREIGN KEY([idEmpresa])
REFERENCES [dbo].[Empresas] ([idEmpresa])
GO
ALTER TABLE [dbo].[Prestamos] CHECK CONSTRAINT [FK_Prestamos_Empresas]
GO
ALTER TABLE [dbo].[Salida]  WITH CHECK ADD  CONSTRAINT [FK_Salida_Empresas] FOREIGN KEY([idEmpresa])
REFERENCES [dbo].[Empresas] ([idEmpresa])
GO
ALTER TABLE [dbo].[Salida] CHECK CONSTRAINT [FK_Salida_Empresas]
GO
ALTER TABLE [dbo].[SalidaUniforme]  WITH CHECK ADD  CONSTRAINT [FK_SalidaUniforme_Salida] FOREIGN KEY([idSalida])
REFERENCES [dbo].[Salida] ([idSalida])
GO
ALTER TABLE [dbo].[SalidaUniforme] CHECK CONSTRAINT [FK_SalidaUniforme_Salida]
GO
ALTER TABLE [dbo].[SalidaUniforme]  WITH CHECK ADD  CONSTRAINT [FK_SalidaUniforme_Uniformes] FOREIGN KEY([idUniforme])
REFERENCES [dbo].[Uniformes] ([idUniforme])
GO
ALTER TABLE [dbo].[SalidaUniforme] CHECK CONSTRAINT [FK_SalidaUniforme_Uniformes]
GO
ALTER TABLE [dbo].[Usuarios]  WITH CHECK ADD  CONSTRAINT [FK_Usuarios_Roles] FOREIGN KEY([idRol])
REFERENCES [dbo].[Roles] ([idRol])
GO
ALTER TABLE [dbo].[Usuarios] CHECK CONSTRAINT [FK_Usuarios_Roles]
GO
USE [master]
GO
ALTER DATABASE [prestameDB] SET  READ_WRITE 
GO
