﻿using System.Web;
using System.Web.Optimization;

namespace PrestameApp
{
    public class BundleConfig
    {
        // Para obtener más información sobre Bundles, visite http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js"));


            bundles.Add(new ScriptBundle("~/bundles/template").Include(
                      //"~/Scripts/pace.min.js",
                      "~/Scripts/wow.min.js",
                      "~/Scripts/jquery.nicescroll.js",
                      "~/Scripts/jquery.app.js"
                      ));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/css/bootstrap.min.css",
                      "~/Content/css/bootstrap-reset.css",
                      "~/Content/css/animate.css",
                      "~/Content/assets/font-awesome/css/font-awesome.css",
                      "~/Content/assets/ionicon/css/ionicons.min.css",
                      "~/Content/css/style.css",
                      "~/Content/css/helper.css",
                      "~/Content/css/style-responsive.css"));
        }
    }
}
