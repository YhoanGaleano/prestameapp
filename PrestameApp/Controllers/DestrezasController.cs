﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using PrestameApp.Models;
using PrestameApp.Tags;
using PrestameApp.Enums;

namespace PrestameApp.Controllers
{

    [Autenticado]
    [Permiso(Modulo = ModulosEnum.Modulo_Destrezas)]
    public class DestrezasController : Controller
    {
        PrestameDBContext db = new PrestameDBContext();

        // GET: Personas
        public ActionResult Index()
        {
            var destrezas = db.Destrezas.ToList();
            return View(destrezas);
        }
    }
}