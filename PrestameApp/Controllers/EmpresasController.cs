﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PrestameApp.Models;
using PrestameApp.Tags;

namespace PrestameApp.Controllers
{

    [Autenticado]
    public class EmpresasController : Controller
    {
        private PrestameDBContext db = new PrestameDBContext();

        [Permiso(Modulo = Enums.ModulosEnum.Listar_Empresas)]
        // GET: Empresas
        public ActionResult Index()
        {
            var empresas = db.Empresas.Include(e => e.Usuarios);
           
            return View(empresas.ToList());
        }


        // GET: Empresas/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Empresas empresas = db.Empresas.Find(id);
            if (empresas == null)
            {
                return HttpNotFound();
            }
            return View(empresas);
        }

        [Permiso(Modulo = Enums.ModulosEnum.Crear_Empresa)]
        // GET: Empresas/Create
        public ActionResult Create()
        {
            ViewBag.idUsuario = new SelectList(db.Usuarios, "idUsuario", "email");
            return View();
        }

        // POST: Empresas/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [Permiso(Modulo = Enums.ModulosEnum.Crear_Empresa)]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "idEmpresa,nit,razonSocial,telefono,nombreResponsable,idUsuario,estado")] Empresas empresas)
        {
            if (ModelState.IsValid)
            {
                db.Empresas.Add(empresas);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.idUsuario = new SelectList(db.Usuarios, "idUsuario", "email", empresas.idUsuario);
            return View(empresas);
        }

        [Permiso(Modulo = Enums.ModulosEnum.Editar_Empresa)]
        // GET: Empresas/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Empresas empresas = db.Empresas.Find(id);
            if (empresas == null)
            {
                return HttpNotFound();
            }
            ViewBag.idUsuario = new SelectList(db.Usuarios, "idUsuario", "email", empresas.idUsuario);
            return View(empresas);
        }

        // POST: Empresas/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [Permiso(Modulo = Enums.ModulosEnum.Editar_Empresa)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "idEmpresa,nit,razonSocial,telefono,nombreResponsable,idUsuario,estado")] Empresas empresas)
        {
            if (ModelState.IsValid)
            {
                db.Entry(empresas).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.idUsuario = new SelectList(db.Usuarios, "idUsuario", "email", empresas.idUsuario);
            return View(empresas);
        }

        [Permiso(Modulo = Enums.ModulosEnum.Inactivar_Empresa)]
        // GET: Empresas/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Empresas empresas = db.Empresas.Find(id);
            if (empresas == null)
            {
                return HttpNotFound();
            }
            return View(empresas);
        }

        [Permiso(Modulo = Enums.ModulosEnum.Inactivar_Empresa)]
        // POST: Empresas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Empresas empresas = db.Empresas.Find(id);
            db.Empresas.Remove(empresas);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        //[Autenticado]
        //public ActionResult CrearEmpresas()
        //{
        //    return View();
        //}

        //[Autenticado]
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult CrearEmpresas(Empresas empresa)
        //{
        //    try
        //    {
        //        ICryptoService cryptoService = new PBKDF2();
        //        var salt = cryptoService.GenerateSalt();
        //        var contraseniaEncriptada = cryptoService.Compute(empresa.Usuarios.contrasenia, salt);
        //        empresa.Usuarios.contrasenia = contraseniaEncriptada;
        //        empresa.Usuarios.contraseniaSalt = salt;


        //        db.Usuarios.Add(empresa.Usuarios);
        //        db.SaveChanges();

        //        empresa.idUsuario = empresa.Usuarios.idUsuario;
        //        empresa.estado = 1;

        //        db.Empresas.Add(empresa);
        //        db.SaveChanges();

        //        return RedirectToAction("Index");

        //    }
        //    catch (Exception ex)
        //    {

        //    }

        //    return View(empresa);
        //}
    }
}
