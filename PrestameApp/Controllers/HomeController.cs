﻿using PrestameApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using SimpleCrypto;

using System.Web.Security;

using PrestameApp.Tags;
using System.Net.Mail;
using System.Net;

using System.Data.Entity;

using PrestameApp.ViewModels;
using PrestameApp.Helpers;

namespace PrestameApp.Controllers
{


    public class HomeController : Controller
    {
        PrestameDBContext db = new PrestameDBContext();

        [Autenticado]
        public ActionResult Index()
        {
            //Usuarios user = new Usuarios();
            //user.email = "andres45@gmail.com";
            //user.usuario = "caldas";
            //user.contrasenia = "pollo";
            //user.contraseniaSalt = "123456";
            //user.idRol = 1;
            //db.Usuarios.Add(user);
            //db.SaveChanges();


            var listaUsuarios = db.Usuarios.ToList();
            ViewBag.listaUsuarios = listaUsuarios;

            return View();
        }

        [Autenticado]
        public ActionResult Perfil()
        {
            var idUsuario = SessionHelper.obtenerUsuarioID();
            var usuario = db.Usuarios.Where(item => item.idUsuario == idUsuario).FirstOrDefault();

            return View(usuario);
        }

        [HttpPost]
        [Autenticado]
        [ValidateAntiForgeryToken]
        public ActionResult Perfil([Bind(Include = "idUsuario, usuario, email, imagen")]Usuarios persona)
        {
            if (persona.imagen != null)
            {
                string pic = "usuario_"+persona.idUsuario + System.IO.Path.GetExtension(persona.imagen.FileName);
                string urlImagen = "/Content/img/perfiles/" + pic;
                string path = System.IO.Path.Combine(
                                       Server.MapPath("~/Content/img/perfiles"), pic);
                persona.imagen.SaveAs(path);


                var usuario = db.Usuarios.Find(persona.idUsuario);
                db.Entry(usuario).State = EntityState.Modified;

                usuario.urlImagen = urlImagen;
                db.SaveChanges();


                persona.urlImagen = urlImagen;

            }
            return View(persona);
        }

        [NoLogin]
        public ActionResult Login()
        {
            return View();
        }

        [NoLogin]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login([Bind(Include = "usuario, contrasenia, recordarme")] Usuarios persona)
        {
            if (ModelState.IsValidField("usuario") && ModelState.IsValidField("contrasenia") && ModelState.IsValidField("recordarme"))
            {

                var usuariodb = db.Usuarios.Where(item => item.usuario == persona.usuario).FirstOrDefault();

                if (usuariodb != null)
                {
                    ICryptoService cryptoService = new PBKDF2();
                    var salt = usuariodb.contraseniaSalt;
                    var contraseniaEncriptada = cryptoService.Compute(persona.contrasenia, salt);

                    if (cryptoService.Compare(contraseniaEncriptada, usuariodb.contrasenia))
                    {
                        FormsAuthentication.SetAuthCookie(usuariodb.idUsuario.ToString(), persona.recordarme);
                        var viewModel = SessionHelper.CrearViewModelLogin(usuariodb.idUsuario);
                        Session["infoUsuario"] = viewModel;

                        return RedirectToAction("Index");
                    }
                    else
                    {
                        ModelState.AddModelError("", "El usuario o contraseña no coinciden");
                    }
                }
                else
                {
                    ModelState.AddModelError("", "El usuario o contraseña no coinciden");
                }

            }

            return View(persona);
        }

        [Autenticado]
        public ActionResult LogOut()
        {
            if (Session["infoUsuario"] != null)
            {
                Session.RemoveAll();
            }

            FormsAuthentication.SignOut();
            return RedirectToAction("Login");
        }

        //Habilitar en gmail para envio de correo
        //https://www.google.com/settings/security/lesssecureapps

        [NoLogin]
        public ActionResult RecuperarContrasenia()
        {
            return View();
        }


        [NoLogin]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult RecuperarContrasenia(Usuarios usuarioRecuperar)
        {
            if (ModelState.IsValidField("usuario"))
            {
                var persona = db.Usuarios.Where(item => item.usuario == usuarioRecuperar.usuario).FirstOrDefault();

                if (persona != null)
                {
                    string emailSalida = "adsi957129@gmail.com";
                    string contraseniaSalida = "wilderno";
                    string emailDestino = persona.email;

                    //Creando una contraseña aleatoria para enviar al correo y que la persona pueda ingresar
                    string contraseniaNueva = RandomPassword.Generate(10, PasswordGroup.Uppercase, PasswordGroup.Lowercase, PasswordGroup.Special, PasswordGroup.Numeric);

                    //Aca estamos encriptando la contraseña genera con el algoritmo de encriptacion que teniamos en la base de datos para el usuario consultado
                    //https://github.com/shawnmclean/SimpleCrypto.net
                    ICryptoService cryptoService = new PBKDF2();
                    string contraseniaEncriptada = cryptoService.Compute(contraseniaNueva, persona.contraseniaSalt);

                    //Estamos indicandole a entity framework que vamos a modificar una propiedad del objeto consultado
                    db.Entry(persona).State = EntityState.Modified;
                    persona.contrasenia = contraseniaEncriptada;

                    try
                    {
                        //Guarde en la base de datos la contraseña modifica
                        db.SaveChanges();
                        EmailEnriquecido(emailSalida, contraseniaSalida, emailDestino, contraseniaNueva);
                        return RedirectToAction("Login");
                    }
                    catch (Exception ex)
                    {

                    }



                }
                else
                {
                    ModelState.AddModelError("", "El usuario no existe");
                }
            }

            return View(usuarioRecuperar);
        }

        public void EmailSencillo(string emailSalida, string contraseniaSalida, string emailDestino, string contraseniaNueva)
        {
            string asunto = "Recuperación de contraseña - PrestameApp";
            string mensaje = string.Format("Por favor ingrese con esta contraseña: {0}", contraseniaNueva);


            var smtp = new SmtpClient();
            {
                smtp.Host = "smtp.gmail.com";
                //smtp.Port = 587;
                smtp.Port = 25;
                smtp.EnableSsl = true;
                smtp.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                smtp.Credentials = new NetworkCredential(emailSalida, contraseniaSalida);
                smtp.Timeout = 90000;
            }

            try
            {
                smtp.Send(emailSalida, emailDestino, asunto, mensaje);
            }
            catch (Exception ex)
            {

            }
        }

        public void EmailEnriquecido(string emailSalida, string contraseniaSalida, string emailDestino, string contraseniaNueva)
        {

            string asunto = "Recuperación de contraseña - PrestameApp";

            System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage();
            msg.To.Add(emailDestino);

            msg.From = new MailAddress(emailSalida, "PrestameApp", System.Text.Encoding.UTF8);
            msg.Subject = asunto;
            msg.SubjectEncoding = System.Text.Encoding.UTF8;
            msg.Body = string.Format("Por favor ingrese con esta contraseña: <b>{0}</b>", contraseniaNueva);
            msg.BodyEncoding = System.Text.Encoding.UTF8;
            msg.IsBodyHtml = true;
            msg.Priority = System.Net.Mail.MailPriority.High;
            //msg.CC.Add("correo@misena.edu.co");
            //msg.Bcc.Add("correo@misena.edu.co");
            //msg.Bcc.Add("correo@misena.edu.co");


            //msg.Attachments.Add(new Attachment(ruta));

            //Aquí es donde se hace lo especial
            SmtpClient client = new SmtpClient();
            client.Credentials = new System.Net.NetworkCredential(emailSalida, contraseniaSalida);
            //client.Port = 587;
            client.Port = 25;
            client.Host = "smtp.gmail.com";
            client.EnableSsl = true; //Esto es para que vaya a través de SSL que es obligatorio con Gmail
            try
            {
                client.Send(msg);
            }
            catch (System.Net.Mail.SmtpException ex)
            {
                Console.WriteLine(ex.Message);
                Console.ReadLine();
            }
        }
    }
}