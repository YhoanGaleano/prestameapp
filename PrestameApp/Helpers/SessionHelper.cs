﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using PrestameApp.ViewModels;
using PrestameApp.Models;
using PrestameApp.Enums;
using System.Web.Security;

namespace PrestameApp.Helpers
{
    public class SessionHelper
    {
        public static bool EstaUtenticado()
        {
            if (!HttpContext.Current.User.Identity.IsAuthenticated)
            {
                return false;
            }
            else
            {
                if (ExisteSesion() == false)
                {
                    var user_id = Convert.ToInt32(HttpContext.Current.User.Identity.Name);
                    var viewModel = CrearViewModelLogin(user_id);
                    HttpContext.Current.Session["infoUsuario"] = viewModel;
                }
            }

            return HttpContext.Current.User.Identity.IsAuthenticated;
        }

        public static bool ExisteSesion()
        {
            return HttpContext.Current.Session["infoUsuario"] == null ? false : true;
        }

        public static LoginViewModel CrearViewModelLogin(int user_id)
        {
            var viewModel = new LoginViewModel();

            using (PrestameDBContext db = new PrestameDBContext())
            {
                var usuario = db.Usuarios.Include("Roles").Include("Empleados").Include("Empresas").Where(x => x.idUsuario == user_id).FirstOrDefault();

                var modulos = (from m in db.Modulo
                               join mr in db.ModuloRol
                               on m.idModulo equals mr.idModulo
                               where mr.idRol == usuario.idRol
                               select m
                               ).ToList();

                viewModel.idUsuario = usuario.idUsuario;
                viewModel.usuario = usuario.usuario;
                viewModel.idRol = usuario.idRol;
                viewModel.nombreRol = usuario.Roles.nombre;
                viewModel.urlImagen = usuario.urlImagen;

                if (usuario.idRol == (int)RolesEnum.Empresa)
                {
                    viewModel.nombreUsuario = usuario.Empresas.FirstOrDefault().razonSocial;
                }
                else if (usuario.idRol == (int)RolesEnum.Empleado)
                {
                    viewModel.nombreUsuario = usuario.Empleados.FirstOrDefault().nombre;
                }
                else
                {
                    viewModel.nombreUsuario = usuario.usuario;
                }

                viewModel.modulos = modulos;
                viewModel.menu = CrearMenu(viewModel.modulos);
            }

            return viewModel;
        }

        public static int obtenerUsuarioID()
        {
            int userID = 0;
            if (HttpContext.Current.User != null && HttpContext.Current.User.Identity is FormsIdentity)
            {
                userID = Convert.ToInt32(HttpContext.Current.User.Identity.Name);
            }
            return userID;
        }

        public static LoginViewModel obtenerViewModel()
        {
            LoginViewModel viewModel = null;
            if (HttpContext.Current.Session["infoUsuario"] != null && HttpContext.Current.User != null && HttpContext.Current.User.Identity is FormsIdentity)
            {
                viewModel = (LoginViewModel)HttpContext.Current.Session["infoUsuario"];
            }
            return viewModel;
        }

        public static bool AutorizadoModulo(int idModulo)
        {
            bool tienePermisos = false;
            var viewModel = obtenerViewModel();

            using (PrestameDBContext db = new PrestameDBContext())
            {
                var modulos = (from mr in db.ModuloRol
                               where mr.idRol == viewModel.idRol && mr.idModulo == idModulo
                               && mr.Modulo.estado == true
                               select mr).ToList();

                tienePermisos = modulos.Count > 0 ? true : false;
            }

            return tienePermisos;
        }

        public static string CrearMenu(List<Modulo> modulos)
        {
            var padres = modulos.Where(item => item.padre == true && item.mostrar == true && item.estado == true).ToList();
            System.Text.StringBuilder menu = new System.Text.StringBuilder();


            foreach (var padre in padres)
            {
                var hijos = modulos.Where(item => item.hijo == true && item.padreId == padre.idModulo && item.mostrar == true && item.estado == true).ToList();

                if (hijos.Count > 0)
                {
                    //Menu padre
                    menu.Append("<li class='has-submenu'>");
                    menu.Append(string.Format("<a href='{0}'><i class='fa {1}'></i> <span class='nav-label'>{2}</span></a>", padre.url, padre.icono, padre.nombre));

                    //submenu o menu hijos
                    menu.Append("<ul class='list-unstyled'>");

                    foreach (var hijo in hijos)
                    {
                       menu.Append(string.Format("<li><a href='{0}'> <span class='nav-label'>{1}</span></a></li>", hijo.url, hijo.nombre));
                    }

                    menu.Append("</ul>");

                    menu.Append("</li>");
                }
                else
                {
                    menu.Append(string.Format("<li><a href='{0}'><i class='fa {1}'></i> <span class='nav-label'>{2}</span></a></li>", padre.url, padre.icono, padre.nombre));
                }
            }

            return menu.ToString();

        }
    }
}