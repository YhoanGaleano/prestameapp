namespace PrestameApp.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Estudios
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Estudios()
        {
            EstudioEmpleado = new HashSet<EstudioEmpleado>();
        }

        [Key]
        public int idEstudio { get; set; }

        [Required]
        [StringLength(100)]
        public string nombre { get; set; }

        [StringLength(200)]
        public string descripcion { get; set; }

        public byte nivelEstudio { get; set; }

        public bool graduado { get; set; }

        [Column(TypeName = "date")]
        public DateTime fechaTerminacion { get; set; }

        [Required]
        [StringLength(100)]
        public string nombreInstitucion { get; set; }

        public bool estado { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<EstudioEmpleado> EstudioEmpleado { get; set; }
    }
}
