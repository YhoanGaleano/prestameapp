namespace PrestameApp.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("DestrezaEmpleado")]
    public partial class DestrezaEmpleado
    {
        [Key]
        public int idDestrezaEmpleado { get; set; }

        public int idEmpleado { get; set; }

        public int idDestreza { get; set; }

        public virtual Destrezas Destrezas { get; set; }

        public virtual Empleados Empleados { get; set; }
    }
}
