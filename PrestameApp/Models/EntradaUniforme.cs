namespace PrestameApp.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("EntradaUniforme")]
    public partial class EntradaUniforme
    {
        [Key]
        public int idEntradaUniforme { get; set; }

        public int idEntrada { get; set; }

        public int idUniforme { get; set; }

        public int cantidad { get; set; }

        public decimal precio { get; set; }

        public virtual Entrada Entrada { get; set; }

        public virtual Uniformes Uniformes { get; set; }
    }
}
