namespace PrestameApp.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Entrada")]
    public partial class Entrada
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Entrada()
        {
            EntradaUniforme = new HashSet<EntradaUniforme>();
        }

        [Key]
        public int idEntrada { get; set; }

        [Column(TypeName = "date")]
        public DateTime fechaEntrada { get; set; }

        public int idUsuario { get; set; }

        public bool estado { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<EntradaUniforme> EntradaUniforme { get; set; }
    }
}
