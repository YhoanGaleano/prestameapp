namespace PrestameApp.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Salida")]
    public partial class Salida
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Salida()
        {
            SalidaUniforme = new HashSet<SalidaUniforme>();
        }

        [Key]
        public int idSalida { get; set; }

        public byte idTipoSalida { get; set; }

        public int idUsuario { get; set; }

        [Column(TypeName = "date")]
        public DateTime fechaSalida { get; set; }

        [Column(TypeName = "date")]
        public DateTime? fechaEnvioTentativa { get; set; }

        [Column(TypeName = "date")]
        public DateTime? fechaDespacho { get; set; }

        [StringLength(100)]
        public string descripcion { get; set; }

        public int idEmpresa { get; set; }

        [StringLength(50)]
        public string urlComprobante { get; set; }

        public byte estado { get; set; }

        public virtual Empresas Empresas { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SalidaUniforme> SalidaUniforme { get; set; }
    }
}
