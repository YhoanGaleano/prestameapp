namespace PrestameApp.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Empresas
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Empresas()
        {
            Empleados = new HashSet<Empleados>();
            Prestamos = new HashSet<Prestamos>();
            Salida = new HashSet<Salida>();
        }

        [Key]
        [Display(Name ="C�digo Empresa")]
        public int idEmpresa { get; set; }

        [Required]
        [StringLength(12)]
        [Display(Name ="NIT Empresa")]
        public string nit { get; set; }

        [Required]
        [StringLength(50)]
        [Display(Name = "Raz�n Social")]
        public string razonSocial { get; set; }

        [Required]
        [StringLength(15)]
        public string telefono { get; set; }

        [Required]
        [StringLength(50)]
        public string nombreResponsable { get; set; }

        public int idUsuario { get; set; }

        public byte estado { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Empleados> Empleados { get; set; }

        public virtual Usuarios Usuarios { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Prestamos> Prestamos { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Salida> Salida { get; set; }
    }
}
