namespace PrestameApp.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class PrestameDBContext : DbContext
    {
        public PrestameDBContext()
            : base("name=PrestameDBContext")
        {
        }

        public virtual DbSet<DestrezaEmpleado> DestrezaEmpleado { get; set; }
        public virtual DbSet<Destrezas> Destrezas { get; set; }
        public virtual DbSet<Empleados> Empleados { get; set; }
        public virtual DbSet<Empresas> Empresas { get; set; }
        public virtual DbSet<Entrada> Entrada { get; set; }
        public virtual DbSet<EntradaUniforme> EntradaUniforme { get; set; }
        public virtual DbSet<EstudioEmpleado> EstudioEmpleado { get; set; }
        public virtual DbSet<Estudios> Estudios { get; set; }
        public virtual DbSet<Modulo> Modulo { get; set; }
        public virtual DbSet<ModuloRol> ModuloRol { get; set; }
        public virtual DbSet<Prestamos> Prestamos { get; set; }
        public virtual DbSet<Roles> Roles { get; set; }
        public virtual DbSet<Salida> Salida { get; set; }
        public virtual DbSet<SalidaUniforme> SalidaUniforme { get; set; }
        public virtual DbSet<Uniformes> Uniformes { get; set; }
        public virtual DbSet<Usuarios> Usuarios { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Destrezas>()
                .Property(e => e.nombre)
                .IsUnicode(false);

            modelBuilder.Entity<Destrezas>()
                .Property(e => e.descripcion)
                .IsUnicode(false);

            modelBuilder.Entity<Destrezas>()
                .HasMany(e => e.DestrezaEmpleado)
                .WithRequired(e => e.Destrezas)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Empleados>()
                .Property(e => e.nombre)
                .IsUnicode(false);

            modelBuilder.Entity<Empleados>()
                .Property(e => e.urlFoto)
                .IsUnicode(false);

            modelBuilder.Entity<Empleados>()
                .HasMany(e => e.DestrezaEmpleado)
                .WithRequired(e => e.Empleados)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Empleados>()
                .HasMany(e => e.EstudioEmpleado)
                .WithRequired(e => e.Empleados)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Empleados>()
                .HasMany(e => e.Prestamos)
                .WithRequired(e => e.Empleados)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Empresas>()
                .Property(e => e.nit)
                .IsUnicode(false);

            modelBuilder.Entity<Empresas>()
                .Property(e => e.razonSocial)
                .IsUnicode(false);

            modelBuilder.Entity<Empresas>()
                .Property(e => e.telefono)
                .IsUnicode(false);

            modelBuilder.Entity<Empresas>()
                .Property(e => e.nombreResponsable)
                .IsUnicode(false);

            modelBuilder.Entity<Empresas>()
                .HasMany(e => e.Empleados)
                .WithRequired(e => e.Empresas)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Empresas>()
                .HasMany(e => e.Prestamos)
                .WithRequired(e => e.Empresas)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Empresas>()
                .HasMany(e => e.Salida)
                .WithRequired(e => e.Empresas)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Entrada>()
                .HasMany(e => e.EntradaUniforme)
                .WithRequired(e => e.Entrada)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<EntradaUniforme>()
                .Property(e => e.precio)
                .HasPrecision(6, 2);

            modelBuilder.Entity<Estudios>()
                .Property(e => e.nombre)
                .IsUnicode(false);

            modelBuilder.Entity<Estudios>()
                .Property(e => e.descripcion)
                .IsUnicode(false);

            modelBuilder.Entity<Estudios>()
                .Property(e => e.nombreInstitucion)
                .IsUnicode(false);

            modelBuilder.Entity<Estudios>()
                .HasMany(e => e.EstudioEmpleado)
                .WithRequired(e => e.Estudios)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Modulo>()
                .Property(e => e.nombre)
                .IsUnicode(false);

            modelBuilder.Entity<Modulo>()
                .Property(e => e.nombreCorto)
                .IsUnicode(false);

            modelBuilder.Entity<Modulo>()
                .Property(e => e.url)
                .IsUnicode(false);

            modelBuilder.Entity<Modulo>()
                .Property(e => e.icono)
                .IsUnicode(false);

            modelBuilder.Entity<Modulo>()
                .HasMany(e => e.ModuloRol)
                .WithRequired(e => e.Modulo)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Prestamos>()
                .Property(e => e.actividades)
                .IsUnicode(false);

            modelBuilder.Entity<Prestamos>()
                .Property(e => e.direccion)
                .IsUnicode(false);

            modelBuilder.Entity<Roles>()
                .Property(e => e.nombre)
                .IsUnicode(false);

            modelBuilder.Entity<Roles>()
                .HasMany(e => e.ModuloRol)
                .WithRequired(e => e.Roles)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Roles>()
                .HasMany(e => e.Usuarios)
                .WithRequired(e => e.Roles)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Salida>()
                .Property(e => e.descripcion)
                .IsUnicode(false);

            modelBuilder.Entity<Salida>()
                .Property(e => e.urlComprobante)
                .IsUnicode(false);

            modelBuilder.Entity<Salida>()
                .HasMany(e => e.SalidaUniforme)
                .WithRequired(e => e.Salida)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SalidaUniforme>()
                .Property(e => e.precio)
                .HasPrecision(6, 2);

            modelBuilder.Entity<Uniformes>()
                .Property(e => e.nombre)
                .IsUnicode(false);

            modelBuilder.Entity<Uniformes>()
                .Property(e => e.urlFotoFrontal)
                .IsUnicode(false);

            modelBuilder.Entity<Uniformes>()
                .Property(e => e.urlFotoTrasera)
                .IsUnicode(false);

            modelBuilder.Entity<Uniformes>()
                .Property(e => e.descripcion)
                .IsUnicode(false);

            modelBuilder.Entity<Uniformes>()
                .Property(e => e.precio)
                .HasPrecision(6, 2);

            modelBuilder.Entity<Uniformes>()
                .HasMany(e => e.EntradaUniforme)
                .WithRequired(e => e.Uniformes)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Uniformes>()
                .HasMany(e => e.SalidaUniforme)
                .WithRequired(e => e.Uniformes)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Usuarios>()
                .Property(e => e.email)
                .IsUnicode(false);

            modelBuilder.Entity<Usuarios>()
                .Property(e => e.usuario)
                .IsUnicode(false);

            modelBuilder.Entity<Usuarios>()
                .Property(e => e.contrasenia)
                .IsUnicode(false);

            modelBuilder.Entity<Usuarios>()
                .Property(e => e.contraseniaSalt)
                .IsUnicode(false);

            modelBuilder.Entity<Usuarios>()
                .HasMany(e => e.Empleados)
                .WithRequired(e => e.Usuarios)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Usuarios>()
                .HasMany(e => e.Empresas)
                .WithRequired(e => e.Usuarios)
                .WillCascadeOnDelete(false);
        }
    }
}
