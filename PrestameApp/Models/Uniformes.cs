namespace PrestameApp.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Uniformes
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Uniformes()
        {
            EntradaUniforme = new HashSet<EntradaUniforme>();
            SalidaUniforme = new HashSet<SalidaUniforme>();
        }

        [Key]
        public int idUniforme { get; set; }

        [Required]
        [StringLength(30)]
        public string nombre { get; set; }

        public byte tipoUniforme { get; set; }

        [Required]
        [StringLength(50)]
        public string urlFotoFrontal { get; set; }

        [Required]
        [StringLength(50)]
        public string urlFotoTrasera { get; set; }

        [StringLength(200)]
        public string descripcion { get; set; }

        public decimal precio { get; set; }

        public int cantidadExistente { get; set; }

        public int cantidadDisponible { get; set; }

        public bool estado { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<EntradaUniforme> EntradaUniforme { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SalidaUniforme> SalidaUniforme { get; set; }
    }
}
