namespace PrestameApp.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Prestamos
    {
        [Key]
        public int idPrestamo { get; set; }

        public int idEmpleado { get; set; }

        public int idEmpresa { get; set; }

        [Column(TypeName = "date")]
        public DateTime fechaInicio { get; set; }

        [Column(TypeName = "date")]
        public DateTime fechaFin { get; set; }

        [Required]
        public string actividades { get; set; }

        [Required]
        [StringLength(50)]
        public string direccion { get; set; }

        public byte estado { get; set; }

        public virtual Empleados Empleados { get; set; }

        public virtual Empresas Empresas { get; set; }
    }
}
