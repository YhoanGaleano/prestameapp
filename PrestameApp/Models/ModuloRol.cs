namespace PrestameApp.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ModuloRol")]
    public partial class ModuloRol
    {
        [Key]
        public int idModuloRol { get; set; }

        public int idModulo { get; set; }

        public int idRol { get; set; }

        public virtual Modulo Modulo { get; set; }

        public virtual Roles Roles { get; set; }
    }
}
