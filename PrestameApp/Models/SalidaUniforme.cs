namespace PrestameApp.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SalidaUniforme")]
    public partial class SalidaUniforme
    {
        [Key]
        public int idSalidaUniforme { get; set; }

        public int idSalida { get; set; }

        public int idUniforme { get; set; }

        public int cantidad { get; set; }

        public decimal precio { get; set; }

        public virtual Salida Salida { get; set; }

        public virtual Uniformes Uniformes { get; set; }
    }
}
