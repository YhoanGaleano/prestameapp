namespace PrestameApp.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("EstudioEmpleado")]
    public partial class EstudioEmpleado
    {
        [Key]
        public int idEstudioEmpleado { get; set; }

        public int idEmpleado { get; set; }

        public int idEstudio { get; set; }

        public virtual Empleados Empleados { get; set; }

        public virtual Estudios Estudios { get; set; }
    }
}
