namespace PrestameApp.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Modulo")]
    public partial class Modulo
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Modulo()
        {
            ModuloRol = new HashSet<ModuloRol>();
        }

        [Key]
        public int idModulo { get; set; }

        [Required]
        [StringLength(50)]
        public string nombre { get; set; }

        [StringLength(20)]
        public string nombreCorto { get; set; }

        [Required]
        [StringLength(40)]
        public string url { get; set; }

        public bool padre { get; set; }

        public bool hijo { get; set; }

        public int? padreId { get; set; }

        [StringLength(20)]
        public string icono { get; set; }

        public bool estado { get; set; }

        public bool mostrar { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ModuloRol> ModuloRol { get; set; }
    }
}
