namespace PrestameApp.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Destrezas
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Destrezas()
        {
            DestrezaEmpleado = new HashSet<DestrezaEmpleado>();
        }

        [Key]
        public int idDestreza { get; set; }

        [Required]
        [StringLength(50)]
        public string nombre { get; set; }

        public byte porcentajeDestreza { get; set; }

        [Required]
        [StringLength(100)]
        public string descripcion { get; set; }

        public bool estado { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DestrezaEmpleado> DestrezaEmpleado { get; set; }
    }
}
