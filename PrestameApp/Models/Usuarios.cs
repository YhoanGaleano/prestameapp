namespace PrestameApp.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Usuarios
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Usuarios()
        {
            Empleados = new HashSet<Empleados>();
            Empresas = new HashSet<Empresas>();
        }

        [Key]
        public int idUsuario { get; set; }

        [Required(ErrorMessage = "El campo Email es requerido para el envio de correos")]
        [StringLength(30)]
        [DataType(DataType.EmailAddress, ErrorMessage = "El campo debe se un correo valido")]
        [EmailAddress(ErrorMessage = "El campo debe se un correo valido")]
        public string email { get; set; }

        [Required]
        [StringLength(20)]
        public string usuario { get; set; }

        [Required]
        [StringLength(100)]
        public string contrasenia { get; set; }

        [Required]
        [StringLength(100)]
        public string contraseniaSalt { get; set; }

        public string urlImagen { get; set; }

        public int idRol { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Empleados> Empleados { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Empresas> Empresas { get; set; }

        public virtual Roles Roles { get; set; }
    }
}
