namespace PrestameApp.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Empleados
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Empleados()
        {
            DestrezaEmpleado = new HashSet<DestrezaEmpleado>();
            EstudioEmpleado = new HashSet<EstudioEmpleado>();
            Prestamos = new HashSet<Prestamos>();
        }

        [Key]
        public int idEmpleado { get; set; }

        [Required]
        [StringLength(40)]
        public string nombre { get; set; }

        [Column(TypeName = "date")]
        public DateTime fechaNacimiento { get; set; }

        public byte genero { get; set; }

        [StringLength(50)]
        public string urlFoto { get; set; }

        public int idUsuario { get; set; }

        public byte estado { get; set; }

        public int idEmpresa { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DestrezaEmpleado> DestrezaEmpleado { get; set; }

        public virtual Empresas Empresas { get; set; }

        public virtual Usuarios Usuarios { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<EstudioEmpleado> EstudioEmpleado { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Prestamos> Prestamos { get; set; }
    }
}
