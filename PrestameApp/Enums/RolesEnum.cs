﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PrestameApp.Enums
{
    public enum RolesEnum
    {
        Administrador = 1,
        Empresa = 2,
        Empleado = 3
    }
}