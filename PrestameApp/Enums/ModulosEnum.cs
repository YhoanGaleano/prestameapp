﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PrestameApp.Enums
{
    public enum ModulosEnum
    {
        Inicio = 0,
        Modulo_Empresa = 9,
        Crear_Empresa = 10,
        Listar_Empresas = 11,
        Editar_Empresa = 12,
        Inactivar_Empresa = 13,
        Modulo_Empleado = 15,
        Crear_Empleado = 16,
        Listar_Empleado = 17,
        Editar_Empleado = 18,
        Inactivar_Empleado = 19,
        Modulo_Destrezas = 21,
        Crear_Destreza = 22,
        Listar_Destreza = 23,
    }
}