﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace PrestameApp.Models
{
    public partial class Usuarios
    {
        [NotMapped]
        public bool recordarme { get; set; }

        [NotMapped]
        public HttpPostedFileBase imagen { get; set; }

    }
}