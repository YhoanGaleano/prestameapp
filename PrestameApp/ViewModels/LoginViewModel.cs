﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using PrestameApp.Models;

namespace PrestameApp.ViewModels
{
    public class LoginViewModel
    {
        public int idUsuario { get; set; }

        public string usuario { get; set; }

        public string nombreUsuario { get; set; }

        public int idRol { get; set; }

        public string nombreRol { get; set; }

        public List<Modulo> modulos { get; set; }

        public string menu { get; set; }

        public string urlImagen { get; set; }
    }
}