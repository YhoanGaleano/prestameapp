﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Web.Mvc;
using System.Web.Security;
using PrestameApp.Enums;

//Importar para manejar las rutas y retornar la ruta
using System.Web.Routing;

//Importar carpeta helper para trabajar con las clases de sesion
using PrestameApp.Helpers;

namespace PrestameApp.Tags
{
    // Si no estamos logeado, regresamos al login
    public class AutenticadoAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);

            bool estaAutenticado = SessionHelper.EstaUtenticado();

            if (!estaAutenticado)
            {

                filterContext.Result =
                    new RedirectToRouteResult
                    (
                        new RouteValueDictionary
                        (
                            new
                            {
                                controller = "Home",
                                action = "Login"
                            }
                        )
                    );
            }
        }
    }

    // Si estamos logeado ya no podemos acceder a la página de Login o recuperación de contraseña (otras que no sea necesario)
    public class NoLoginAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);

            bool estaAutenticado = SessionHelper.EstaUtenticado();

            if (estaAutenticado)
            {
                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new
                {
                    controller = "Home",
                    action = "Index"
                }));
            }
        }
    }

    //Permite sacar a un usuario si no tiene permisos en ese modulo
    public class PermisoAttribute : ActionFilterAttribute
    {
        public ModulosEnum Modulo { get; set; }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);

            if (SessionHelper.AutorizadoModulo((int)this.Modulo) == false)
            {
                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new
                {
                    controller = "Home",
                    action = "Index"
                }));
            }
        }
    }
}